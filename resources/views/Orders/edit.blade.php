@extends('layouts.sidebar')
@section('content')

<h1>Edit a order</h1>
<form method='post' action="{{action('OrdersController@update', $order->id)}}">
  @csrf
  @method('PATCH')

 

<div class="form-group">
    <div class="col-md-2">
      <label class="control-label" for="model"> Engine Code </label></div>
    <div class="col-md-10">
      <input type="text" class="form-control" name="model" value="{{$order->model}}">
    </div>

    <div class="form-group">
    <div class="col-md-2">
      <label class="control-label" for="customer"> Customer </label></div>
    <div class="col-md-10">
      <input type="text" class="form-control" name="customer" value="{{$order->customer}}">
    </div>

    <div class="form-group">
    <div class="col-md-2">
      <label class="control-label" for="Address"> Address </label></div>
    <div class="col-md-10">
      <input type="text" class="form-control" name="address" value="{{$order->address}}">
    </div>

        <div class="form-group">
    <div class="col-md-2">
      <label class="control-label" for="totalPrice">Price Without TAX</label></div>
    <div class="col-md-10">
      <input type="number" class="form-control" name="totalPrice" value="{{$order->totalPrice}}">
    </div>

    <div class="form-group">
    <div class="col-md-2">
      <label class="control-label" for="isPaid"> Paid? </label></div>
    <div class="col-md-10">
      <input type="text" class="form-control" name="isPaid" value="{{$order->isPaid}}">
    </div>

        <div class="form-group">
    <div class="col-md-2">
      <label class="control-label" for="telephone"> Telephone </label></div>
    <div class="col-md-10">
      <input type="number" class="form-control" name="telephone" value="{{$order->telephone}}">
    </div>

    <div class="form-group">
    <div class="col-md-2">
      <label class="control-label" for="deliveryDate"> Date </label></div>
    <div class="col-md-10">
      <input type="date" class="form-control" name="deliveryDate" value="{{$order->deliveryDate}}">
    </div>


    <div class="form-group">
    <div class="col-md-2">
      <label class="control-label" for="remarks"> Remarks </label></div>
    <div class="col-md-10">
      <input type="text" class="form-control" name="remarks" value="{{$order->remarks}}">
    </div>
  
   
  </div>

  <div class="col-md-8">
    <div class="form-group">
      <input type="submit" class="btn btn-success btn-block" name="submit" value="Save Changes">
    </div>
  </div>
</form>




@endsection